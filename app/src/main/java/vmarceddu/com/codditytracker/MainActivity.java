package vmarceddu.com.codditytracker;

import android.os.AsyncTask;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.Console;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    NotificationManagerCompat notificationManager;
    Timer t = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        notificationManager = NotificationManagerCompat.from(this);
        t.scheduleAtFixedRate(new TimerTask() {
              @Override
              public void run() {
                  new RetrieveHTMLTask().execute("https://www.nuitdelinfo.com/nuitinfo/defis2018:start");
              }
          },
        0,
        15000);
    }

    class RetrieveHTMLTask extends AsyncTask<String, Void, String> {

        protected String doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(url.openStream()));

                StringBuilder htmlContent = new StringBuilder();
                String inputLine;
                while ((inputLine = in.readLine()) != null)
                    htmlContent.append(inputLine);
                in.close();

                return htmlContent.toString();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        protected void onPostExecute(String htmlContent) {
            System.out.println(htmlContent);
            Document doc = Jsoup.parse(htmlContent);
            Element coddityDefi = doc.select(".defi").get(14);
            Element podium = coddityDefi.selectFirst(".title #podium");
            Integer podiumCount = podium.childNodeSize();
            if (podiumCount > 0) {
                t.cancel();
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(MainActivity.this, "channelID")
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle("IL Y A LES RESULTATS !!!")
                        .setContentText(podium.html())
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                notificationManager.notify(10, mBuilder.build());

            } 
        }
    }
}
